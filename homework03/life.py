import pygame
from pygame.locals import *
import random
from copy import deepcopy
from typing import List, Tuple

class GameOfLife:

    def __init__(self, width=640, height=480, cell_size=10, speed=10):
        self.width = width
        self.height = height
        self.cell_size = cell_size

        # Устанавливаем размер окна
        self.screen_size = width, height
        # Создание нового окна
        self.screen = pygame.display.set_mode(self.screen_size)

        # Вычисляем количество ячеек по вертикали и горизонтали
        self.cell_width = self.width // self.cell_size
        self.cell_height = self.height // self.cell_size

        # Скорость протекания игры
        self.speed = speed

    def draw_grid(self):
        """ Отрисовать сетку """
        for x in range(0, self.width, self.cell_size):
            pygame.draw.line(self.screen, pygame.Color('black'),
                             (x, 0), (x, self.height))
        for y in range(0, self.height, self.cell_size):
            pygame.draw.line(self.screen, pygame.Color('black'),
                             (0, y), (self.width, y))

    def run(self):
        """ Запустить игру """
        pygame.init()
        clock = pygame.time.Clock()
        pygame.display.set_caption('Game of Life')
        self.screen.fill(pygame.Color('white'))
        self.clist = self.cell_list()

        # Создание списка клеток
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == QUIT:
                    running = False
            self.draw_cell_list(self.clist)
            self.update_cell_list(self.clist)
            self.draw_grid()
            pygame.display.flip()
            clock.tick(self.speed)
        pygame.quit()

    def cell_list(self, randomize=True) -> list:
        """
        Создание списка клеток.
        Клетка считается живой, если ее значение равно 1.
        В противном случае клетка считается мертвой, то
        есть ее значение равно 0.
        Если параметр randomize = True, то создается список, где
        каждая клетка может быть равновероятно живой или мертвой.
        """
        self.clist = []
        clist = [[0 for i in range(self.cell_width)]
                 for j in range(self.cell_height)]
        if randomize:
            clist = [[random.randint(0, 1)for i in range(
                self.cell_width)] for j in range(self.cell_height)]
        self.clist = clist

        return self.clist

    def draw_cell_list(self, clist: list) -> None:
        """
        Отображение списка клеток 'rects' с закрашиванием их в
        соответствующе цвета
        """
        for i in range(len(clist)):
            for j in range(len(clist[i])):
                if clist[i][j] == 1:
                    pygame.draw.rect(self.screen, pygame.Color('green'),
                                     (self.cell_size * j, self.cell_size * i,
                                      self.cell_size, self.cell_size))
                else:
                    pygame.draw.rect(self.screen, pygame.Color('white'),
                                     (self.cell_size * j, self.cell_size * i,
                                      self.cell_size, self.cell_size))

    def get_neighbours(self, cell: tuple) -> list:
        """
        Вернуть список соседних клеток для клетки cell.
        Соседними считаются клетки по горизонтали,
        вертикали и диагоналям, то есть во всех
        направлениях.
        """
        neighbours = []
        xc, yc = cell
        for i in range(xc - 1, xc + 2):
            for j in range(yc - 1, yc + 2):
                if i in range(0, self.cell_height):
                    if j in range(0, self.cell_width):
                        if(i != xc or j != yc):
                            neighbours.append(self.clist[i][j])

        return neighbours

    def update_cell_list(self, cell_list: list) -> list:
        """
        Обновление состояния клеток
        """
        new_clist = []  # type:list
        new_list = deepcopy(cell_list)
        for i in range(self.cell_height):
            for j in range(self.cell_width):
                if cell_list[i][j]:
                    if sum(self.get_neighbours((i, j))) in (2, 3):
                        new_list[i][j] = 1
                    else:
                        new_list[i][j] = 0
                else:
                    if sum(self.get_neighbours((i, j))) == 3:
                        new_list[i][j] = 1
                    else:
                        new_list[i][j] = 0
        self.clist = new_list
        return new_list

if __name__ == '__main__':
    game1 = GameOfLife(1000, 500, 10)
    game1.run()  
