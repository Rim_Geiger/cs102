"""
Реализуем функцию encrypt_caesar()
"""
def encrypt_caesar(plaintext):
    """
    >>> encrypt_caesar("PYTHON")
    'SBWKRQ'
    >>> encrypt_caesar("python")
    'sbwkrq'
    >>> encrypt_caesar("Python3.6")
    'Sbwkrq3.6'
    >>> encrypt_caesar("")
    ''
    """
    ciphertext = ''
    for text in plaintext:
        if 'a' <= text <= 'z' or 'A' <= text <= 'Z':
            write = ord(text) + 3
            if 'x' <= text <= 'z':
                write = ord(text) - 23
            ciphertext += chr(write)
        else:
            ciphertext += text
    return ciphertext

"""
Реализуем функцию decrypt_caesar()
"""
def decrypt_caesar(ciphertext):
    """
    >>> decrypt_caesar("SBWKRQ")
    'PYTHON'
    >>> decrypt_caesar("sbwkrq")
    'python'
    >>> decrypt_caesar("Sbwkrq3.6")
    'Python3.6'
    >>> decrypt_caesar("")
    ''
    """
    plaintext = ''
    for text in ciphertext:
        if 'a' <= text <= 'z' or 'A' <= text <= 'Z':
            write = ord(text) - 3
            if 'a' <= text <= 'c':
                write += 26
            plaintext += chr(write)
        else:
            plaintext += text
    return plaintext