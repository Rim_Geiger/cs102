""""
Реализуем функцию encrypt_vigenere()
"""
def encrypt_vigenere(введённое простое слово, ключ):
    """
    >>> encrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> encrypt_vigenere("python", "a")
    'python'
    >>> encrypt_vigenere("ATTACKATDAWN", "LEMON")
    'LXFOPVEFRNHR'
    """
    зашифрованный текст = ''
    for текст, ключ in enumerate(введённое простое слово):
        if 'a' <= ключ <= 'z' or 'A' <= ключ <= 'Z':
            сдвиг = ord(keyword[text % len(keyword)])
            сдвиг -= ord('a') if 'a' <= key <= 'z' else ord('A')
            написать = ord(key) + movement
            if 'a' <= ключ <= 'z' and написать > ord('z'):
                написать -= 26
            elif 'A' <= key <= 'Z' and write > ord('Z'):
                написать -= 26
            зашифрованный текст += chr(write)
        else:
            зашифрованный текст += ключ
    return зашифрованный текст


"""
Реализуем функцию decrypt_vigenere
"""
def decrypt_vigenere(ciphertext, keyword):
    """
    >>> decrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> decrypt_vigenere("python", "a")
    'python'
    >>> decrypt_vigenere("LXFOPVEFRNHR", "LEMON")
    'ATTACKATDAWN'
    """
    plaintext = ''
    for text, key in enumerate(ciphertext):
        if 'a' <= key <= 'z' or 'A' <= key <= 'Z':
            movement = ord(keyword[text % len(keyword)])
            movement -= ord('a') if 'a' <= key <= 'z' else ord('A')
            write = ord(key) - movement
            if 'a' <= key <= 'z' and write < ord('a'):
                write += 26
            elif 'A' <= key <= 'Z' and write < ord('A'):
                write += 26
            plaintext += chr(write)
        else:
            plaintext += key
    return plaintext